import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class MyProtoHandler extends SimpleChannelInboundHandler<MyProtoPackage> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyProtoPackage pack) throws Exception {

        ServiceInfo.qps.incrementAndGet();
        //System.out.println("ThreadID = " + Thread.currentThread().getId());
        //System.out.println("commandId = " + pack.getCommandId() +  ", sid = " + pack.getSessionId() +
        //        ", server_id = " + pack.getServerId() + ", clientId = " +
        //        pack.getClientId() + ", len = " + pack.getPayload().length);

        ctx.writeAndFlush(pack);
        //Main.taskGroup.execute(new MyTask(ctx, pack));
    }
}

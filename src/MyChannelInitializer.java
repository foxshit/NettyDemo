import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

public class MyChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static MyProtoEncoder protoEncoder = new MyProtoEncoder();
    private static MyLogInHandler logInHandler = new MyLogInHandler();
    private static MyLogOutHandler logOutHandler = new MyLogOutHandler();
    private static MyHeartbeatHandler heartbeatHandler = new MyHeartbeatHandler();



    @Override
    protected void initChannel(SocketChannel channel) throws Exception {

        channel.pipeline().addLast(logInHandler);
        channel.pipeline().addLast(logOutHandler);

        //channel.pipeline().addLast(new MyEchoHandler());

        channel.pipeline().addLast(new IdleStateHandler(5, 5, 10));

        channel.pipeline().addLast(new MyProtoDecoder());
        //channel.pipeline().addLast(protoDecoder);
        channel.pipeline().addLast(protoEncoder);

        channel.pipeline().addLast(heartbeatHandler);

        channel.pipeline().addLast(new MyProtoHandler());

    }
}

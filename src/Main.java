import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.concurrent.TimeUnit;

/**
 *  This is NettyServer Demo
 */

public class Main {

    public static final EventLoopGroup taskGroup = new DefaultEventLoopGroup(8);
    public static MySignalHandler signalHandler = null;

    public static void main(String[] args) {
        System.out.println("This is test....");

        final EventLoopGroup listenGroup = new NioEventLoopGroup(2);
        EventLoopGroup workerGroup = new NioEventLoopGroup(4);




        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(listenGroup, workerGroup);
            b.option(ChannelOption.SO_REUSEADDR, true);
            b.option(ChannelOption.SO_BACKLOG, 1024);

            b.channel(NioServerSocketChannel.class);
            b.childHandler(new MyChannelInitializer());

            listenGroup.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    System.out.println("client_num = " + ServiceInfo.client_num.get() +
                            ", qps = " + ServiceInfo.qps.getAndSet(0) +
                            ", recv_bytes = " + ServiceInfo.recv_bytes.get()+
                            ", send_bytes = " + ServiceInfo.send_bytes.get() );

                    //listenGroup.schedule(this, 1, TimeUnit.SECONDS);
                }
            }, 1, 1, TimeUnit.SECONDS);

            b.bind(9999);
            ChannelFuture f = b.bind(8088).sync();

            signalHandler = new MySignalHandler(f.channel());
            signalHandler.register();

            f.channel().closeFuture().sync();

        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            listenGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

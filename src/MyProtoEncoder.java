import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;


@ChannelHandler.Sharable
public class MyProtoEncoder extends MessageToByteEncoder<MyProtoPackage> {
    @Override
    protected void encode(ChannelHandlerContext ctx, MyProtoPackage pack, ByteBuf out) throws Exception {
        out.writeShort(0x55AA);
        out.writeShort(pack.getPackLen());
        out.writeInt(pack.getCommandId());
        out.writeInt(pack.getSessionId());
        out.writeInt(pack.getServerId());
        out.writeInt(pack.getClientId());
        out.writeBytes(pack.getPayload());
    }
}

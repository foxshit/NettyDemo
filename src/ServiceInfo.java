import java.util.concurrent.atomic.AtomicLong;

public class ServiceInfo {
    public static AtomicLong client_num = new AtomicLong();
    public static AtomicLong qps = new AtomicLong();
    public static AtomicLong recv_bytes = new AtomicLong();
    public static AtomicLong send_bytes = new AtomicLong();
}

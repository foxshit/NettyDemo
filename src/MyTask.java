import io.netty.channel.ChannelHandlerContext;

public class MyTask implements Runnable {

    private ChannelHandlerContext _ctx = null;
    private MyProtoPackage _pack = null;
    public MyTask(ChannelHandlerContext ctx, MyProtoPackage pack) {
        this._ctx = ctx;
        this._pack = pack;
    }

    @Override
    public void run() {
        // TODO: 测试
        this._ctx.writeAndFlush(this._pack);
        //this._ctx.channel().writeAndFlush(this._pack);
    }
}

/****
 *  MagicMaker  55AA  2 byte
 *  PackLen  1 Word   2 byte
 *  CommandId 1 Integer
 *  SID 1 integer  4 byte
 *  ServerID 1 integer
 *  ClientId 1 integer
 *
 */

public class MyProtoPackage {
    private int _sessionId;
    private int _commandId;
    private int _serverId;
    private int _clientId;
    private byte[] _payload;

    public final static int CMD_PING = 1001;
    public final static int CMD_PONG = 1002;

    public MyProtoPackage(int commandId, int sessionId, int serverId, int clientId, byte[] payload) {
        this._commandId = commandId;
        this._sessionId = sessionId;
        this._serverId  = serverId;
        this._clientId  = clientId;
        this._payload   = payload;
    }

    public int getCommandId() {
        return this._commandId;
    }

    public void setCommandId(int cmd) {
        this._commandId = cmd;
    }

    public int getServerId() {
        return  this._serverId;
    }

    public int getSessionId() {
        return  this._sessionId;
    }

    public int getClientId() {
        return this._clientId;
    }

    public byte[] getPayload() {
        return  this._payload;
    }

    int getPackLen() {
        return this._payload.length + 20;
    }

}

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class MyEchoHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ServiceInfo.qps.incrementAndGet();
        ctx.writeAndFlush(msg);
        //ctx.fireChannelRead(msg);
    }
}

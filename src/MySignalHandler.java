import io.netty.channel.Channel;
import sun.misc.Signal;
import sun.misc.SignalHandler;

public class MySignalHandler implements SignalHandler {
    private Channel listenChannel = null;

    public MySignalHandler(Channel ch) {
        this.listenChannel = ch;
    }

    public void register( ) {
        //Signal.handle(new Signal("HUP"), this);
        Signal.handle(new Signal("INT"), this);
        //Signal.handle(new Signal("QUIT"), this);
    }

    @Override
    public void handle(Signal signal) {
        System.out.println("SigalHandler: " + signal);
        listenChannel.close();
    }


}

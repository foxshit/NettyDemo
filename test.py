#!/usr/bin/env python
#-*- encoding:utf8 -*-

import socket
import time
import struct
import random


def make_package(sid, command, servid, clientid, payload): 
    packlen = len(payload) + 20
    cmd = struct.pack('>H', 0x55AA) + struct.pack('>H', packlen) + struct.pack(">I", command) +  struct.pack(">I", sid) + struct.pack('>I', servid) + struct.pack('>I', clientid) + payload

    return cmd

def main():
    print "to test ...."

    s = socket.create_connection(("127.0.0.1", 8088))
    #s.setblocking(False)
    
    send_bytes = 0
    recv_bytes = 0

    i = 1
    for i in xrange(1, 9999999):
        sid = i + 1
        cid = random.randint(1, 999999)
        servId = random.randint(1, 999999)
        clientid = random.randint(1, 999999)
        cmd = make_package(sid, cid, servId, clientid, "fdsafdsafdsafdsafd%d-%d-%d" %(cid, servId, clientid))
        
        s.send(cmd)
        send_bytes += len(cmd)

        data = s.recv(16*1024);
        recv_bytes += len(data)


    print "send_bytes = ", send_bytes, " recv_bytes = ", recv_bytes

    # for i in xrange(1, 1000):  
    #     s.send('\x55\xAA\x00\x15\x00\x00\x00\x08\x11\x22\x33\x44\x11\x22\x33\x44\x11\x22\x33\x44\x99')
    #     s.send('\x55\xAA\x00\x16\x00\x00\x00\x0A\x11\x22\x33\x44')

    #     time.sleep(2)

    #     s.send('\x11\x22\x33\x44\x11\x22\x33\x44\x99\x98')


    s.shutdown(1)
    time.sleep(1)

    s.close()



if __name__ == '__main__':
    main()
    